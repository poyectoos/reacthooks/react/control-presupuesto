import { Fragment } from 'react';
import PropTypes from 'prop-types';

import { tipoAlerta } from '../helpers/alerta';

const Resumen = ({ presupuesto, restante }) => {
  return (
    <Fragment>
      <div className="alert alert-primary">
        <b>Presupuesto: { presupuesto }</b>
      </div>
      <div className={ `alert ${tipoAlerta(presupuesto, restante)} ` }>
        <b>Restante: { restante }</b>
      </div>
    </Fragment>
  );
};

Resumen.propTypes = {
  presupuesto: PropTypes.number.isRequired,
  restante: PropTypes.number.isRequired
}

export default Resumen;