import PropTypes from 'prop-types';

import Gasto from './Gasto';

const Gastos = ({ gastos }) => {
  return (
    <div className="gastos-realizados">
      <h2>Listado</h2>
      <ul>
        {
          gastos.map(gasto => (
            <Gasto
              key={gasto._id}
              gasto={gasto}
            />
          ))
        }
      </ul>
    </div>
  );
};

Gastos.propTypes = {
  gastos: PropTypes.array.isRequired
}

export default Gastos;