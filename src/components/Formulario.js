import { useState } from 'react';
import { v4 } from "uuid";
import PropTypes from 'prop-types';

import Error from "./Error";

const Formulario = ({ actualizarGasto, actualizarIniciar }) => {

  // States forma separada
  const [nombre, actualizarNombre] = useState('');
  const [cantidad, actualizarCantidad] = useState(0);
  // State ppara manejar el error
  const [error, actualizarError] = useState(false);

  // Agregamos el gasto
  const enviarGasto = (e) => {
    e.preventDefault();
    // Validamos
    if (cantidad <= 0 || isNaN(cantidad) || nombre === '') {
      actualizarError(true);
      return;
    }
    actualizarError(false);

    // Cnstruir el gasto
    const gasto = {
      _id: v4(),
      nombre,
      cantidad
    }

    // Enviamos al state principal
    actualizarGasto(gasto);
    actualizarIniciar(true);

    // Reiniciamos form
    actualizarNombre('');
    actualizarCantidad(0);

  }

  return (
    <div>
      <form onSubmit={enviarGasto}>
        <h2>Agregar gasto</h2>

        {
          error
            ?
          <Error mensaje="Todos los campos son obligatorios o presupuesto no valido" />
            :
          null
        }

        <div className="campo">
          <label htmlFor="nombre">Nombre</label>
          <input
            type="text"
            name="nombre"
            className="u-full-width"
            value={nombre}
            onInput={e => actualizarNombre(e.target.value)}
          />
        </div>
        <div className="campo">
          <label htmlFor="cantidad">Cantidad</label>
          <input
            type="number"
            pattern="[0-9]"
            name="cantidad"
            className="u-full-width"
            value={cantidad}
            onInput={e => actualizarCantidad(Number(e.target.value))}
          />
        </div>

        <input
          type="submit"
          value="Agregar"
          className="u-full-width"
        />
      </form>
    </div>
  );
};

Formulario.propTypes = {
  actualizarGasto:  PropTypes.func.isRequired,
  actualizarIniciar: PropTypes.func.isRequired
}

export default Formulario;