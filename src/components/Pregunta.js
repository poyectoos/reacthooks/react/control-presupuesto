import { Fragment, useState } from 'react';
import Error from "./Error";
import PropTypes from 'prop-types';

const Pregunta = ({ actualizarPresupuesto, actualizarRestante }) => {

  // Definimos state para el presupuesto
  const [cantidad, actualizarCantidad] = useState(0);
  // State ppara manejar el error
  const [error, actualizarError] = useState(false);

  // Guardamos el presupuesto
  const definirPresupuesto = (e) => {
    actualizarCantidad(parseInt(e.target.value), 10);
  }

  // Submit del presupuesto
  const enviarCantidad = e => {
    e.preventDefault();
    // Validar presupuesto
    if (cantidad <=0 || isNaN(cantidad)) {
      actualizarError(true);
      return;
    }
    actualizarError(false);
    // Agregamos la cantidad al state global alv
    actualizarPresupuesto(cantidad);
    actualizarRestante(cantidad);
  }

  return (
    <Fragment>
      <h2>¿Cuál es tu presupuesto?</h2>
      {
        error
          ?
        <Error mensaje="El presupuesto es incorrecto" />
          :
        null
      }
      <form onSubmit={enviarCantidad}>
        <label htmlFor="presupuesto">Presupuesto</label>
        <input
          type="number"
          name="presupuesto"
          className="u-full-width"
          pattern="[0-9]"
          onInput={definirPresupuesto}
        />

        <input
          type="submit"
          value="Definir"
          className="u-full-width button button-primary"
        />
      </form>
    </Fragment>
  );
};

Pregunta.propTypes = {
  actualizarPresupuesto: PropTypes.func.isRequired,
  actualizarRestante: PropTypes.func.isRequired
}

export default Pregunta;