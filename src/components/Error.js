import PropTypes from 'prop-types';

const Error = ({ mensaje }) => {
  return (
    <p className="alert alert-danger error">
      <b>Ups!!!</b> { mensaje }...
    </p>
  );
};

Error.propTypes = {
  mensaje: PropTypes.string.isRequired
}

export default Error;