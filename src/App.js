import { useState, useEffect } from "react";

import Pregunta from "./components/Pregunta";
import Formulario from "./components/Formulario";
import Gastos from "./components/Gastos";
import Resumen from './components/Resumen';

function App() {

  // State para el dinerito
  const [ presupuesto, actualizarPresupuesto ] = useState(0);
  // State para el restante
  const [ restante, actualizarRestante ] = useState(0);
  // State para almacenar los gastos
  const [ gastos, actualizarGastos ] = useState([]);
  // State para recibir el gasto
  const [gasto, actualizarGasto ] = useState({});
  /// State para saber si ya se puede agregar gastos
  const [ iniciar , actualizarIniciar ] = useState(false);
  // Use efect que actualiza el restante
  useEffect(() => {
    if (iniciar) {
      // Agrega nuevo gasto
      actualizarGastos([
        gasto,
        ...gastos
      ]);

      // Calcula restante
      actualizarRestante(restante-gasto.cantidad);

      // marca que ya se puede agregar
      actualizarIniciar(false);
    }
  }, [gasto, gastos, iniciar, restante])
  return (
    <div className="container">
      <header>
        <h1>Gasto semanal</h1>
        <div className="contenido contenido-principal">
          {
            presupuesto === 0
              ?
            <Pregunta
              actualizarPresupuesto={actualizarPresupuesto}
              actualizarRestante={actualizarRestante}
            />
              :
            <div className="row">
              <div className="one-half column">
                <Formulario
                  actualizarGasto={actualizarGasto}
                  actualizarIniciar={actualizarIniciar}
                />
              </div>
              <div className="one-half column">
                <Gastos
                  gastos={gastos}
                />
                <Resumen
                  presupuesto={presupuesto}
                  restante={restante}
                />
              </div>
            </div>
          }
        </div>
      </header>
    </div>
  );
}

export default App;
