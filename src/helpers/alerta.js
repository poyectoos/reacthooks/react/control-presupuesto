export const tipoAlerta = (presupuesto, restante) => {
  let tipo;
  if (presupuesto*0.25 > restante) {
    tipo = 'danger';
  } else if (presupuesto*0.5 > restante) {
    tipo = 'warning';
  } else {
    tipo = 'success';
  }
  return 'alert-'+tipo;
}